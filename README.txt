
Copyright 2008 Joshua Benner

Description
-----------
Default Roles allows multiple roles to be granted (and/or revoked) by each
group. This is useful if you wish to use membership in certain groups to grant
site-wide roles/permissions. For instance, you could have an "Editors" group,
whose members will be automatically granted several appropriate roles at the
time they join the "Editors" group.

NOTE
----
Leaving a group will only revoke a permission per the og default roles
configuration is no other group grants that role. This means that if two
groups both grant a role, and a user leaves a group that revokes the role, the
user will retain the role.  This is signifigant when you consider the case
where the user subsequently leaves the second group that does not revoke the
role -- the user will retain the role.

Because of this it is often good practice to only have one group grant a role
if the role must be removed upon a user leaving a group -- or at least, every
group that grants a role should also revoke it. This is done to avoid some
users having unexpected permissions after poorly-planned assignment of roles
via group membership.

Features
--------
* Any group may grant any role upon a user joining the group
* Any group may revoke any role upon a user leaving the group

Prerequisites
-------------
* Drupal 5
* Organic Groups

Installation
------------
Copy the og_default_roles folder to the appropriate modules folder, such as
sites/all/modules, then enable the module in Site Building -> Modules.

Author
------
Joshua Benner (joshbenner /at\ gmail.com)

Thanks to my employer, Rock River Star (rockriverstar.com)