<?php
/**
 * @file OG Default Roles Module
 */

/**
 * Implementation of hook_perm()
 */
function og_default_roles_perm() {
  return array('administer og default roles');
}

function og_default_roles_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/og/default-roles',
      'title' => t('OG Default Roles'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('og_default_roles_settings'),
      'type' => MENU_NORMAL_ITEM,
      'access' => user_access('administer og default roles'),
    );
  }
  return $items;
}

/**
 * Generates settings form
 * 
 * @ingroup forms
 * @ingroup themable
 */
function og_default_roles_settings() {
  $groups = og_all_groups_options();
  $r = db_query("SELECT rid,name FROM {role}");
  while ($role = db_fetch_object($r)) {
    $roles[$role->rid] = $role->name;
  }
  $dr = db_query("SELECT nid,rid,`revoke` FROM {og_default_roles}");
  while ($drole = db_fetch_object($dr)) {
    $default[$drole->nid][$drole->rid] = 1;
    $revoke[$drole->nid][$drole->rid] = $drole->revoke;
  }
  foreach ($groups as $gid=>$groupName) {
    $g = "group_$gid";
    $form[$g] = array(
      '#type' => 'fieldset',
      '#title' => t('Default Roles for @group', array('@group'=>$groupName)),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    foreach ($roles as $rid=>$roleName) {
      if ($rid != 1 && $rid != 2) {
        $form[$g]["{$gid}_grants_{$rid}"] = array(
          '#type' => 'checkbox',
          '#title' => $roleName,
          '#default_value' => $default[$gid][$rid],
        );
        $form[$g]["{$gid}_revokes_{$rid}"] = array(
          '#type' => 'checkbox',
          '#title' => t('(revoke !role upon leaving group)', array('!role'=>$roleName)),
          '#default_value' => $revoke[$gid][$rid],
          '#attributes' => array('style' => 'margin-left: 30px;'),
        );
      }
    }
  }
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  );
  return $form;
}

/**
 * Processes settings form
 */
function og_default_roles_settings_submit($form_id, $values) {
  foreach ($values as $fieldName=>$value) {
    if (preg_match('/(\d+)_grants_(\d+)/', $fieldName, $matches) && $value) {
      list(, $gid, $rid) = $matches;
      $revoke = $values["{$gid}_revokes_{$rid}"];
      $defaults[] = '('.(integer)$gid.','.(integer)$rid.','.(integer)$revoke.')';
    }
  }
  db_query("DELETE FROM {og_default_roles}");
  if ($defaults) {
    $vals = implode(',', $defaults);
    db_query("INSERT INTO {og_default_roles} (nid,rid,`revoke`) VALUES $vals");
  }
  drupal_set_message(t('Default Group Roles saved'));
}

/**
 * Implementation of hook_og()
 */
function og_default_roles_og($op, $nid, $uid, $args = array()) {
  switch ($op) {
    case 'user insert':
      $roles = og_default_roles_group_grants($nid);
      if ($args['is_active']) {
        $account = user_load(array('uid' => $uid));
        $uroles = array_keys($account->roles);
        $needs = array();
        foreach ($roles as $rid=>$rname) {
          if (!in_array($rid, $uroles)) {
            $needs[$rid] = $rname;
          }
        }
        if ($needs) {
          $edit['roles'] = $account->roles + $needs;
          user_save($account, $edit);
          $group = node_load($nid);
          $vars = array(
            '!user' => $account->name,
            '!group' => $group->title,
            '!roles' => implode(', ', $needs),
          );
          watchdog('og_default_roles', t('!user was granted these roles for joining !group: !roles', $vars));
          cache_clear_all($account->uid.':', 'cache_menu', TRUE);
        }
      }
      break;
    case 'user delete':
      if ($revokes = og_default_roles_group_revokes($nid)) {
        $revoke_rids = implode(',', array_keys($revokes));
        $user_groups = array_keys(og_get_subscriptions($uid));
        $ug_nids = implode(',', $user_groups);
        // retrieve rids of any roles that other groups user is a member of grants -- we won't remove those
        $r = db_query("SELECT rid FROM {og_default_roles} WHERE rid IN ($revoke_rids) AND nid != %d AND nid IN ($ug_nids)", $nid);
        $keep = array();
        while ($k = db_fetch_object($r)) {
          $keep[] = $k->rid;
        }
        if ($revoke = array_diff(array_keys($revokes), $keep)) {
          $account = user_load(array('uid' => $uid));
          if ($_revoke = array_intersect(array_keys($account->roles), $revoke)) {
            $edit['roles'] = array_diff_key($account->roles, array_flip($_revoke));
            user_save($account, $edit);
            $group = node_load($nid);
            $vars = array(
              '!user' => $account->name,
              '!group' => $group->title,
              '!roles' => implode(', ', array_intersect_key($revokes, array_flip($_revoke))),
            );
            watchdog('og_default_roles', t('!user left !group and has lost these roles as a result: !roles', $vars));
            cache_clear_all($account->uid.':', 'cache_menu', TRUE);
          }
        }
      }
      break;
  }
}

/**
 * Returns list of roles that the indicated group grants
 *
 * @param integer $nid
 *  ID of group node
 */
function og_default_roles_group_grants($nid) {
  static $grants = array();
  if (!isset($grants[$nid])) {
    $r = db_query("SELECT dr.nid,r.rid,r.name FROM {og_default_roles} dr INNER JOIN {role} r ON (r.rid = dr.rid) WHERE dr.nid=%d", $nid);
    while ($g = db_fetch_object($r)) {
      $grants[$nid][$g->rid] = $g->name;
    }
  }
  return $grants[$nid];
}

function og_default_roles_group_revokes($nid) {
  static $revokes = array();
  if (!isset($revokes[$nid])) {
    $r = db_query("SELECT dr.nid,r.rid,r.name FROM {og_default_roles} dr INNER JOIN {role} r ON (r.rid = dr.rid) WHERE dr.`revoke`=1 AND dr.nid=%d", $nid);
    while ($g = db_fetch_object($r)) {
      $revokes[$nid][$g->rid] = $g->name;
    }
  }
  return $revokes[$nid];
}